import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservationPage } from '../reservation/reservation';


@NgModule({
  declarations: [ReservationPage],
  imports: [IonicPageModule.forChild(ReservationPage)],
})
export class HomePageModule { }