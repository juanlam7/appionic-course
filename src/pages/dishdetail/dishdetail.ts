import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ActionSheetController, ModalController } from 'ionic-angular';
import { Dish } from '../../shared/dish';
import { Comment } from '../../shared/comment';
import { MyApp } from '../../app/app.component';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { CommentPage } from '../comment/comment';

/**
 * Generated class for the DishdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dishdetail',
  templateUrl: 'dishdetail.html',
})
export class DishdetailPage {
  dish: Dish;
  errMess: string;
  avgstars: string;
  numcomments: number;
  favorite: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public actionSheetController: ActionSheetController,
    public modalCtrl: ModalController,
    @Inject('BaseURL') private BaseURL,
    private favoriteservice: FavoriteProvider,
    private toastCtrl: ToastController) {
    this.dish = navParams.get('dish');
    this.favorite = this.favoriteservice.isFavorite(this.dish.id);
    this.config();
  }

  config() {
    this.numcomments = this.dish.comments.length;
  let total = 0;
  this.dish.comments.forEach(comment => total += comment.rating);
  this.avgstars = (total / this.numcomments).toFixed(2);
  }

  addToFavorites() {
    console.log('Adding to Favorites', this.dish.id);
    this.favorite = this.favoriteservice.addFavorite(this.dish.id);
    this.toastCtrl.create({
      message: 'Dish ' + this.dish.id + ' added as favorite successfully',
      position: 'middle',
      duration: 3000}).present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DishdetailPage');
  }


  showActionSheet() {
    const actionSheet =  this.actionSheetController.create({
      title: 'Select Actions',
      buttons: [{
        text: 'Add to Favorites',
        handler: () => {
          this.addToFavorites();
        }
      }, {
        text: 'Add Comment',
        handler: () => {
          this.openComment();
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    actionSheet.present();
  }

  openComment() {

    let modal = this.modalCtrl.create(CommentPage, {dishComment: this.dish});
    modal.onDidDismiss(data => {
          console.log('MODAL DATA', data);
          this.dish = data;
          this.config();
        });
        modal.present();
  }

}


