import { Component, Inject, ViewChild } from '@angular/core';
import { Dish } from '../../shared/dish';
import { DishProvider } from '../../providers/dish/dish';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../../shared/comment';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {


  @ViewChild('fform') commentFormDirective;
  comment:FormGroup;
  commentCopy:Comment;
  dish:Dish;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,) {
      this.dish = (navParams.get('dishComment'));
      this.comment = this.formBuilder.group({
      rating: 5,
      author: ['', Validators.required],
      comment: ['', Validators.required],
    });

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  onSubmit() {
    console.log(this.comment.value);
   
    this.commentCopy = this.comment.value;
    this.commentCopy.date = new Date().toISOString();

    this.dish.comments.push(this.commentCopy);
    this.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.dish);
  }

}
