import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentPage } from '../comment/comment';


@NgModule({
  declarations: [CommentPage],
  imports: [IonicPageModule.forChild(CommentPage)],
})
export class HomePageModule { }